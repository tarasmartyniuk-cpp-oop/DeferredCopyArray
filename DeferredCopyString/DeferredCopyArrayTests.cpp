#include <test_utils.h>
#include <DeferredCopyArray.h>
#include <cassert>
#include <algorithm>

#pragma region Helpers

struct LifeMonitor
{
	static int TimesConstructed;
	static int TimesDestructed;

	LifeMonitor()
	{
		++TimesConstructed;
	}

	~LifeMonitor()
	{
		++TimesDestructed;
	}

	static void Reset()
	{
		TimesConstructed = 0;
		TimesDestructed = 0;
	}
};
int LifeMonitor::TimesConstructed = 0;
int LifeMonitor::TimesDestructed = 0;

const int DouglasSize = 42;

int* DouglasAlloc()
{
	return  new int[DouglasSize] {42, 42, 42, 42, 42};
}

using MonitorArr = DeferredCopyArray<LifeMonitor>;
#pragma endregion

void Ctor_CreatesEquivalentArray()
{
	LOG_START

	const DeferredCopyArray<int> arr(DouglasAlloc(), DouglasSize);

	assert(arr[0] == 42);

	LogPassed();
}

void Ctor_NoReallocation()
{
	LOG_START

	MonitorArr arr (new LifeMonitor[DouglasSize], DouglasSize);

	assert(LifeMonitor::TimesConstructed == DouglasSize);

	LogPassed();
}

#pragma region Copy

void CopyConstructor_CopyHasSameElements()
{
	LOG_START

	const DeferredCopyArray<int> arr (DouglasAlloc(), DouglasSize);
	auto const copy (arr);

	assert(copy[1] == arr[1]);
	LogPassed();
}

void CopyConstructor_CopyHasSameElementAddresses()
{
	LOG_START

	const DeferredCopyArray<int> arr (DouglasAlloc(), DouglasSize);
	auto const copy (arr);

	assert(&copy[1] == &arr[1]);
	LogPassed();
}

void CopyConstructor_NoReallocation()
{
	LOG_START

	LifeMonitor::Reset();
	MonitorArr arr (new LifeMonitor[DouglasSize], DouglasSize);
	auto copy (arr);

	assert(LifeMonitor::TimesConstructed == DouglasSize);
	LogPassed();
}

void Assignment_NoReallocation()
{
	LOG_START
	LifeMonitor::Reset();

	MonitorArr arr (new LifeMonitor[DouglasSize], DouglasSize);
	MonitorArr copy;
	copy = arr;

	assert(LifeMonitor::TimesConstructed == DouglasSize);
	LogPassed();
}

void Assignment_DeletesData_IfOwningLastCopy()
{
	LOG_START
	LifeMonitor::Reset();

	MonitorArr arr (new LifeMonitor[DouglasSize], DouglasSize);
	MonitorArr copy(new LifeMonitor[DouglasSize], DouglasSize);
	copy = arr;

	assert(LifeMonitor::TimesDestructed == DouglasSize);
	LogPassed();
}

#pragma endregion

#pragma region Indexing

void ConstIndexing_ReturnsElementAtI()
{
	LOG_START

	const DeferredCopyArray<int> arr(DouglasAlloc(), DouglasSize);

	int const& ref = arr[1];
	int i = arr[1];

	assert(ref == 42);
	assert(i == 42);

	LogPassed();
}

void ConstIndexing_NoReallocation()
{
	LOG_START
	LifeMonitor::Reset();

	MonitorArr const arr (new LifeMonitor[DouglasSize], DouglasSize);

	LifeMonitor const& ref = arr[1];
	LifeMonitor i = arr[1];

	assert(LifeMonitor::TimesConstructed == DouglasSize);

	LogPassed();
}

void NonConstIndexing_IfOnlyCopy_NoReallocation()
{
	LOG_START
	LifeMonitor::Reset();

	MonitorArr onlyArr (new LifeMonitor[DouglasSize], DouglasSize);

	LifeMonitor const i = onlyArr[1];
	LifeMonitor& ref = onlyArr[1];

	assert(LifeMonitor::TimesConstructed == DouglasSize);
	LogPassed();
}

void NonConstIndexing_IfSharedCopy_ReallocatesNewCopy()
{
	LOG_START
	LifeMonitor::Reset();

	MonitorArr arr (new LifeMonitor[DouglasSize], DouglasSize);
	auto copy(arr);

	LifeMonitor& ref = arr[1];

	assert(LifeMonitor::TimesConstructed == 2 * DouglasSize);

	LogPassed();
}

void WriteIndexing_OtherCopiesUnaffected()
{
	LOG_START
	DeferredCopyArray<int> arr (DouglasAlloc(), DouglasSize);
	int orig = arr[0];
	auto copy(arr);

	copy[0] = -1;

	int afterCopy = arr[0];
	assert(afterCopy == orig);

	LogPassed();
}

void WriteIndexing_AfterNonConstRefReturned_CopyReallocates()
{
	LOG_START
	LifeMonitor::Reset();
	MonitorArr arr (new LifeMonitor[DouglasSize], DouglasSize);
	LifeMonitor& ref = arr[0];

	auto copy(arr);
	assert(LifeMonitor::TimesConstructed == 2 * DouglasSize);
	LogPassed();
}
#pragma endregion

void AllTests()
{
	Ctor_CreatesEquivalentArray();
	Ctor_NoReallocation();

	CopyConstructor_CopyHasSameElements();
	CopyConstructor_CopyHasSameElementAddresses();

	CopyConstructor_NoReallocation();

	Assignment_NoReallocation();
	Assignment_DeletesData_IfOwningLastCopy();

	ConstIndexing_ReturnsElementAtI();
	ConstIndexing_NoReallocation();

	NonConstIndexing_IfOnlyCopy_NoReallocation();
	NonConstIndexing_IfSharedCopy_ReallocatesNewCopy();
	WriteIndexing_OtherCopiesUnaffected();
	WriteIndexing_AfterNonConstRefReturned_CopyReallocates();
	LogAllPassed();
}