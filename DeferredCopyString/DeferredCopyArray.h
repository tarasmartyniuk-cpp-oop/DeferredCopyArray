#pragma once
#include <cassert>

template <class T>
class DeferredCopyArray
{
	struct SharedData;
	class ElementProxy;
public:
#pragma region lifetime
	// array inited with null
	// using object before assigning another array is yields undefined behaviour
	explicit DeferredCopyArray()
		: m_sharedData()
	{}

	DeferredCopyArray(T* array, int length)
		: m_sharedData(new SharedData (array, length))
	{
		if(!array)
		{
			throw std::logic_error("array is null");
		}
	}

	// let's forget about move semantics for this assignment)
	DeferredCopyArray(DeferredCopyArray const& other)
		: m_sharedData(nullptr)
	{
		// TODO: create a copy if other is not shareable
		if(other.m_sharedData->Shareable)
		{
			m_sharedData = other.m_sharedData;
			++m_sharedData->RefCount;
		}
		else
		{
			m_sharedData = new SharedData(*other.m_sharedData);
		}
	}

	~DeferredCopyArray()
	{
		DetachData();
	}

	DeferredCopyArray& operator=(DeferredCopyArray const& other)
	{
		DetachData();

		m_sharedData = other.m_sharedData;
		++m_sharedData->RefCount;

		return *this;
	}
#pragma endregion

	T const& operator[](int index) const
	{
 		Check(index);
		return m_sharedData->Array[index];
	}

	ElementProxy operator[](int index)
	{
		return ElementProxy(*this, index);
	}

	int GetLength()
	{
		return m_sharedData->Length;
	}
private:
	SharedData* m_sharedData;

	// delete's if we we're holding the last ref
	void DetachData()
	{
		if(! m_sharedData)
		{
			return;
		}

		if(m_sharedData->RefCount == 1)
		{
			delete m_sharedData;
		}
		m_sharedData = nullptr;
	}

	T& Read(int index)
	{
		Check(index);
		return m_sharedData->Array[index];
	}

	void Check(int index) const
	{
		if(index >= m_sharedData->Length || index < 0)
		{
			throw std::logic_error("out of bounds");
		}
	}
};

#include <SharedData.h>
#include <ElementProxy.h>
