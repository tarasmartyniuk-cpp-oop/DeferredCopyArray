#pragma once

#pragma once
#include <string>
#include <functional>
#include <iostream>

#define LOG_START LogStart(__FUNCTION__);

template <class TException>
bool ExpressionThrows(const std::function<void()>& expression)
{
    try
    {
        expression();
        return false;
    }
    catch(const TException& e)
        { return true; }
}

inline void LogPassed()
{
	std::cout << "\tPASSED\n";
}

inline void LogStart(const std::string& testName)
{
	std::cout << testName << '\n';
}

inline void LogAllPassed()
{
	std::cout << "passed all tests!\n";
}

void AllTests();


