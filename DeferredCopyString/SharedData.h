#pragma once

template <class T>
struct DeferredCopyArray<T>::SharedData
{
	// this should be private, yes
	std::unique_ptr<T[]> Array;
	int Length;
	// true if data object was compromised - non-const reference returned to client
	bool Shareable;
	int RefCount;

	DeferredCopyArray<T>::SharedData()
	: SharedData(nullptr, 0)
	{}

	DeferredCopyArray<T>::SharedData(DeferredCopyArray<T>::SharedData const& other)
		: SharedData(CopyAlloc(other.Array.get(), other.Length), other.Length)
	{}

	// takes ownership of array
	DeferredCopyArray<T>::SharedData(T* array, int length)
		: Array(array),
		  Length(length),
		  Shareable(true),
		  RefCount(1)
	{}
	// if not shared returns itself,
	// else returns new instance
	SharedData* GetNotSharedCopy()
	{
		if(RefCount == 1)
		{
			Shareable = false;
			return this;
		}
		--RefCount;
		auto* newInstance = new SharedData(*this);
		newInstance->Shareable = false;
		return newInstance;
	}

	static T* CopyAlloc(T* arr, int length)
	{
		assert(arr);
		assert(length > 0);

		T* res = new T[length];

		std::copy(arr, arr + length, res);

		return res;
	}
};

