#pragma once
/*
* the proxy brakes the c++ 11 auto type deduction!
*	we need to provide the full element type 
*	so that compiler can decide which of the type cast operators to use.
*	e.g auto& ref = arr[0]; will be deduced as Proxy& - no room for negotiation!
*
*
* the proxy handles only non-const indexing of array
*/
template <class TElem>
class DeferredCopyArray<TElem>::ElementProxy
{
	friend class DeferredCopyArray<TElem>;
	using ProxyShip = DeferredCopyArray<TElem>;
public:
	TElem const* operator&() const
	{
		m_proxyShip.m_sharedData = m_proxyShip.m_sharedData->GetNotSharedCopy();
		return &m_proxyShip.Read(m_index);
	}

	operator TElem& ()
	{
		m_proxyShip.m_sharedData = m_proxyShip.m_sharedData->GetNotSharedCopy();
		return m_proxyShip.Read(m_index);
	}

	// totally overshadowed by TElem& overload!
	//operator TElem () const
	//{
	//	return m_proxyShip.Read(m_index);
	//}

	ElementProxy& operator=(TElem const& c)
	{
		m_proxyShip.m_sharedData = m_proxyShip.m_sharedData->GetNotSharedCopy();
		m_proxyShip.Read(m_index) = c;
		return *this;
	}

	bool operator ==(const ElementProxy& other) const
	{
		return static_cast<TElem>(*this) == static_cast<TElem>(other);
	}

private:
	ElementProxy(ProxyShip& proxyShip, int index)
		: m_proxyShip(proxyShip), m_index(index)
	{}

	ProxyShip& m_proxyShip;
	int const m_index;
};
